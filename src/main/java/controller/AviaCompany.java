package controller;
import model.*;
import view.UserMenu;
import java.util.Scanner;
public class AviaCompany {
    private Airport manager;
    private UserMenu menu;
    private Scanner scan;
    public AviaCompany() {
        manager = new Airport();
        menu = new UserMenu();
        scan = new Scanner(System.in);
    }
    public void showInConsole() {
        menu.output();
        manager.totalLoadFactor();
        manager.totalSits();
        menu.askForOrder();
        showSortResult();
        menu.askForFuelRange();
        showPlanesFromFuelRange();
        menu.chooseEngine();
        runEngine();
    }
    private void showSortResult() {
        while(true) {
            int numberOfOrder = scan.nextInt();
            if (numberOfOrder == 1) {
                manager.sortBySpeed();
                break;
            }
            if (numberOfOrder == 2) {
                manager.sortBySits();
                break;
            } else {
                System.out.println("You entered incorrect number");
            }
        }
    }
    private void showPlanesFromFuelRange() {
        while (true) {
            int startOfFuelRange = scan.nextInt();
            int endOfFuelRange = scan.nextInt();
            if ((startOfFuelRange > 0) && (startOfFuelRange < endOfFuelRange)) {
                manager.sortByFuelRange(startOfFuelRange, endOfFuelRange);
                break;
            }
            System.out.println("Incorrect number");
        }
    }
    private void runEngine() {
        while (true) {
            int typeOfEngine = scan.nextInt();
            if ((typeOfEngine > 0) && (typeOfEngine < 4)) {
                manager.useEngine(typeOfEngine);
                break;
            }
            System.out.println("Incorrect number");
        }
    }
}
