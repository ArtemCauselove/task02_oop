package model;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
public class Airport {
    public Airport() {
        createAviaCompany();
    }
    private  ArrayList<Plane> aviaBase;
    private  ArrayList<Plane> createAviaCompany() {
        aviaBase = new ArrayList<>();
        aviaBase.add(new Jmini("Jmini549", 400, 70, 9, 7));
        aviaBase.add(new Jmini("Jmini548", 150, 45, 15, 8));
        aviaBase.add(new Jmini("Jmini650", 200, 60, 7, 10));
        aviaBase.add(new CornPlaneAH2("CornPlaneAH2LightEdition", 10, 10, 2, 4));
        aviaBase.add(new CornPlaneAH2("CornPlaneAH2StandartEdition", 20, 30, 3, 2));
        aviaBase.add(new CornPlaneAH2("CornPlaneAH2FullEdition", 50, 20, 5, 5));
        aviaBase.add(new Boing747("Boing747-75", 500, 100, 20, 50));
        aviaBase.add(new Boing747("Boing747-91", 1000, 200, 50, 15));
        aviaBase.add(new Boing747("Boing747-23", 1500, 150, 30, 200));
        return aviaBase;
    }
    private int sumOfSits;
    private int sumOfLoadFactor;
    public void totalLoadFactor() {
        for (Plane currentPlane : createAviaCompany()) {
            sumOfLoadFactor = currentPlane.loadCapacity + sumOfLoadFactor;
        }
        System.out.println("Total of load capacity : " + sumOfLoadFactor);
    }
    public void totalSits() {
        for (Plane currentPlane : aviaBase) {
            sumOfSits = currentPlane.sit + sumOfSits;
        }
        System.out.println("Total of all sits : " + sumOfSits);
    }
    public void sortBySpeed() {
        Comparator<Plane> comparator = Comparator.comparing(obj -> obj.speed);
        Collections.sort(aviaBase, comparator);
        for (Plane currentPlane : aviaBase) {
            System.out.println(currentPlane);
        }
    }
    public void sortBySits() {
        Comparator<Plane> comparator = Comparator.comparing(obj -> obj.sit);
        Collections.sort(aviaBase, comparator);
        for (Plane currentPlane : aviaBase) {
            System.out.println(currentPlane);
        }
    }
    public void sortByFuelRange(int getStartOfFuelRange, int getEndOfFuelRange) {
        int startOfFuelRange = getStartOfFuelRange;
        int endOfFuelRange = getEndOfFuelRange;
        for (Plane currentPlane : aviaBase) {
            if ((currentPlane.fuel > startOfFuelRange) && (currentPlane.fuel < endOfFuelRange)) {
                System.out.println(currentPlane);
            }
        }
    }
    public void useEngine(int typeOfEngine) {
        switch (typeOfEngine) {
            case 1:
                System.out.println(Boing747.hugeEngine.runBoing747Engine());
                break;
            case 2:
                System.out.println(Jmini.smallEngine.runjMiniEngine());
                break;
            case 3:
                System.out.println(CornPlaneAH2.standartEngine.runCornPlaneEngine());
                break;
        }
    }
}
