package model;
public class Boing747 extends Plane {
    static BoingEngine hugeEngine;
    Boing747() {
    }
    Boing747(String name, int loadCapacity, int speed, int fuel, int sit) {
        this.name = name;
        this.loadCapacity = loadCapacity;
        this.speed = speed;
        this.fuel = fuel;
        this.sit = sit;
        hugeEngine = new BoingEngine();
    }
    @Override
    public String toString() {
        return "Name: '" + this.name
                + "'|| Load Capacity: '" + this.loadCapacity
                + "'|| speed: '" + this.speed + "'"
                + "'|| fuel: '" + this.fuel + "'"
                + "'|| sits: '" + this.sit + "'";
    }
}
class BoingEngine extends Boing747 {
     String runBoing747Engine() {
        return "This is how Boing engine works";
    }
}


