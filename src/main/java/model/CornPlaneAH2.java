package model;
public class CornPlaneAH2 extends Plane {
    static CornPlaneEngine standartEngine;
    CornPlaneAH2() {
    }
    CornPlaneAH2(String name, int loadCapacity, int speed, int fuel, int sit) {
        this.name = name;
        this.loadCapacity = loadCapacity;
        this.speed = speed;
        this.fuel = fuel;
        this.sit = sit;
        standartEngine = new CornPlaneEngine();
    }
    @Override
    public String toString() {
        return "Name: '" + this.name
                + "'|| Load Capacity: '" + this.loadCapacity
                + "'|| speed: '" + this.speed + "'"
                + "'|| fuel: '" + this.fuel + "'"
                + "'|| sits: '" + this.sit + "'";
    }
}
class CornPlaneEngine extends CornPlaneAH2{
     String runCornPlaneEngine() {
        return "This is how CornPlane engine works";
    }
}

