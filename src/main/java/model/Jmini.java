package model;
public class Jmini extends Plane {
    static JMiniEngine smallEngine;
    Jmini() {
    }
    Jmini(String name, int loadCapacity, int speed, int fuel, int sit) {
        this.name = name;
        this.loadCapacity = loadCapacity;
        this.speed = speed;
        this.fuel = fuel;
        this.sit = sit;
        smallEngine = new JMiniEngine();
    }
    @Override
    public String toString() {
        return "Name: '" + this.name
                + "'|| Load Capacity: '" + this.loadCapacity
                + "'|| speed: '" + this.speed + "'"
                + "'|| fuel: '" + this.fuel + "'"
                + "'|| sits: '" + this.sit + "'";
    }
}
class JMiniEngine extends Jmini {
    String runjMiniEngine() {
        return "This is how Jmini engine works";
    }
}
